<?php
    error_reporting(0);
    require_once "config.php";
?>

<html>
<head>
    <meta charset="utf-8">

    <script src="js/jquery-3.1.1.js" ></script>
    <script src="semantic/semantic.js" ></script>
    <link rel="stylesheet" type="text/css" href="semantic/semantic.css">
    <script src='js/tinymce/tinymce.js'></script>

    <script src='js/moment-with-locales.js'></script>
    <script src='js/jquery-ui.js'></script>
    <script src='js/fullcalendar.js'></script>
    <script src='js/th.js'></script>


    <link href='css/fullcalendar.css' rel='stylesheet' />
    <link href='css/fullcalendar.print.css' rel='stylesheet' media='print' />

    <style type="text/css">
        button.li {
            background-color: Transparent;
            background-repeat: no-repeat;
            border: none;
            cursor: pointer;
            overflow: hidden;
            outline: none;
            padding: 0px;
        }
    </style>
    <style type="text/css">
    	#wrap {
    		width: 100%;
    		margin: 0 auto;
    	}

    	#external-events {
    		float: left;
    		width: 15%;
    		border: 1px solid #ccc;
            border-radius: 4px;
    		text-align: left;
            text-align: center;
    	}

    	#external-events .fc-event {
    		margin: 10px 0;
    		cursor: pointer;
    	}

    	#calendar {
    		float: right;
    		width: 84%;
    	}
    </style>
    <title>Semantic Content</title>
</head>
<body>
    <div class="">
        <?php require_once "top-menu.php"; ?>

        <div class="ui grid" style="padding: 10px 20px 0px 20px">
            <div class="row">
                <div class="<?php echo $HAS_LEFT_MENU ? "two" : "zero"; ?> wide column">
                    <?php if ($HAS_LEFT_MENU) require_once "left-menu.php"; ?>
                </div>
                <div class="<?php echo $HAS_LEFT_MENU ? "fourteen" : "sixteen"; ?> wide column">
                    <?php require_once PAGE_TO($file_names); ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<?php unset($_POST); ?>
<script type="text/javascript">
    $('.message .close').on('click', function() {
        $(this).closest('.message').transition('fade');
    });
    $('select.dropdown').dropdown();
</script>
