<?php
date_default_timezone_set('Asia/Bangkok');

//////// DB ////////
require_once "class_Result.php";
require_once "db_config.php";
require_once "class_ContentStatus.php";


//////// PAGE ////////
// at index 0 is default
$PAGES = array("home", "register", "login", "profile", "writer", "staff", "editor", "manager", "professional", "visitor", "contents", "content-detail", "edit-content", "calendar", "news", "new-news", "new-file", "files", "logout", "wall", "read");
$file_names = array("home.php", "register.php", "login.php", "profile.php", "writer.php", "staff.php", "editor.php", "manager.php", "professional.php", "visitor.php", "contents.php", "content-detail.php", "edit-content.php", "calendar.php", "news.php", "new-news.php", "new-file.php", "files.php",
"", "wall.php", "read.php");
$has_left_menu_list = array(0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
0, 0, 0);

$G = $_GET;
$P = $G["page"];
if (is_null($P) || !in_array($P, $PAGES)) {
    $P = $PAGES[0];
}

$HAS_LEFT_MENU = false;
if (PAGE_TO($has_left_menu_list)) {
    $HAS_LEFT_MENU = true;
}

$ID = GC("id");
if (isset($ID)) {
    $NAME = SQL("SELECT name FROM users WHERE id = $ID", false)->res["name"];
    $TYPE = SQL("SELECT type FROM users WHERE id = $ID", false)->res["type"];
}

function PAGE_TO($st) {
    global $P;
    global $PAGES;
    return $st[array_search($P, $PAGES)];
}

// get current url
function CUR() {
    return $_SERVER['REQUEST_URI'];
}

// get params
function PAR() {
    return $_SERVER['QUERY_STRING'];
}

// PAGE() if go to default page
function PAGE($page = null) {
    if (is_null($page)) {
        global $PAGES;
        $page = $PAGES[0];
    }
    return "?page=" . $page;
}

// go to with GET param
function PAGE_PAR($page, $params = array()) {
    header("Location:" . GET_PAGE_PAR($page, $params));
    exit();
}

function GET_PAGE_PAR($page, $params = array()) {
    $p = "";
    $k = array_keys($params);
    for ($i = 0; $i < count($params); $i++) {
        $p .= "&" . $k[$i] . "=" . $params[$k[$i]];
    }
    return PAGE($page) . $p;
}



//////// COOKIE ////////
function LOGOUT() {
    RC("id");
    header("Location:" . PAGE());
    exit();
}

function GC($k) {
    if (!isset($_COOKIE[$k])) {
        return null;
    }

    return $_COOKIE[$k];
}

function SC($k, $v) {
    setcookie($k, $v, time() + 7 * 3600);
}

function RC($k) {
    setcookie($k, "", time() - 3600);
}



//////// UTILITY ////////
// $type 0 = negative, 1 = positive
function MESSAGE($type = 0, $title = "", $message = "", $id = "", $hidden = false) {
    $t = ($type == 0 ? "negative" : "positive");
    $i = ($id == "" ? "" : "id='$id'");
    $h = ($hidden == true ? "transition hidden" : "");

    echo <<< EOD
    <div class="ui $t message $h" $i>
    <i class="close icon"></i>
    <div class="header">
    $title
    </div>
    <p>$message</p>
    </div>
EOD;
}

function HUMANTIME($dtString) {
    $then = DateTime::createFromFormat("Y-m-d H:i:s", $dtString)->format('U');
    $now = (new DateTime("now"))->format('U');

    $time = $now - $then;
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    }

}




?>
