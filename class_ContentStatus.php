<?php
class ContentStatus {
    public static $STATUS = array(
        0 => "เขียนบทความ",
        1 => "เขียนบทความ",
        10 => "เขียนบทความ",
        100 => "รอกำหนดวันส่ง",
        11 => "เขียนบทความ",
        101 => "รอกำหนดวันส่ง",
        110 => "รอกำหนดผู้ทรงคุณวุฒิ",
        111 => "ส่งบทความ",
        2 => "ประเมินผลแล้ว",
        3 => "เผยแพร่บทความ"
    );

    public static function getStatusText($id) {
        return ContentStatus::$STATUS[ContentStatus::getStatus($id)];
    }

    public static function getStatus($id) {
        $r = SQL("SELECT status, due_date, professional_id, approved_date, publish_date FROM contents WHERE id = $id", false);

        if ($r->res["status"] == 0 || $r->res["due_date"] == "" || $r->res["professional_id"] == "0") {
            return ($r->res["status"] == 0 ? 0 : 100) + ($r->res["due_date"] == "" ? 0 : 10) + ($r->res["professional_id"] == "0" ? 0 : 1);
        }
        else if ($r->res["approved_date"] == "") {
            return 111;
        }
        else if ($r->res["publish_date"] != "") {
            return 3;
        }
        else if ($r->res["status"] == 1) {
            return 2;
        }
    }


    public static $MAIN_STATUS = array(
        0 => array(0, 1, 10, 11),   //"เขียนบทความ"
        1 => array(100, 101),       //"รอกำหนดวันส่ง"
        2 => array(110),            //"รอกำหนดผู้ทรงคุณวุฒิ"
        3 => array(111),            //"ส่งบทความ"
        4 => array(2),              //"ประเมินผลแล้ว"
        5 => array(3)               //"ประเมินผลแล้ว"
    );
}
?>
