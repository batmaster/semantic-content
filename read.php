<?php
    $g_id = $_GET["id"];

    if ($_POST["form"] == "comment") {
        $content = $_POST["content"];
        $content_id = $g_id;
        $commentator_id = $ID;
        $r = SQL("INSERT INTO comments (content, content_id, commentator_id, added_date) VALUES ('$content', $content_id, $commentator_id, NOW())");

        if ($r->affected_rows != 1) {
            MESSAGE(0, "เพิ่มความเห็นไม่สำเร็จ", "กรุณาลองใหม่");
        }
    }

    $r = SQL("SELECT ct.id, ct.title, ct.content, (SELECT u.name FROM users u WHERE u.id = ct.author_id) author, ct.publish_date FROM contents ct
    WHERE ct.status = 1 AND ct.due_date != '' AND ct.professional_id != 0 AND ct.approved_date != '' AND ct.publish_date != '' AND ct.id = $g_id", false);

    if (!isset($g_id) || $r->num_rows == 0) {
        PAGE_PAR("wall");
    }

    $c = SQL("SELECT (SELECT u.name FROM users u WHERE u.id = co.commentator_id) name, co.content, co.added_date FROM comments co WHERE co.content_id = $g_id");
?>

<div class="ui container">
        <div class="ui card" style="width: 100%;">
            <div class="content">
                <div class="header"><?= $r->res["title"] ?></a></div>
                <div class="meta"><?= $r->res["author"] ?></div>
                <div class="meta"><?= $r->res["publish_date"] ?></div>
                <div class="description" style="display: inline;">
                    <?= html_entity_decode($r->res["content"]) ?>
                </div>
            </div>
        </div>

    <div class="ui comments" style="max-width: 100%;">
        <h3 class="ui dividing header">ความคิดเห็น</h3>

        <?php for ($i = 0; $i < count($c->res); $i++) : ?>
            <div class="comment">
                <div class="content">
                    <a class="author"><?= $c->res[$i]["name"] ?></a>
                    <div class="metadata">
                        <span class="date"><?= $c->res[$i]["added_date"] ?></span>
                    </div>
                    <div class="text">
                        <?= $c->res[$i]["content"] ?>
                    </div>
                </div>
            </div>
        <?php endfor; ?>

        <form class="ui reply form" id="comment" method="POST" action="<?= GET_PAGE_PAR("read", array("id" => $g_id)) ?>">
            <input type="hidden" name="form" value="comment">
            <div class="field">
                <textarea name="content"></textarea>
            </div>
            <button class="ui blue labeled submit icon button" type="submit">
                <i class="icon edit"></i> เพิ่มความเห็น
            </button>
        </form>
    </div>
</div>
