<?php
    if ($_POST["form"] == "login") {
        $email = $_POST["email"];
        $password = MD5($_POST["password"]);
        $r = SQL("SELECT id, status FROM users WHERE email = '$email' AND password = '$password'", false);
        print_r($r);
        if ($r->num_rows == 0) {
            MESSAGE(0, "ไม่พบบัญชีผู้ใช้", "กรุณาลองอีเมล์อื่นหรือตรวจสอบรหัสผ่านอีกครั้ง");
        }
        else if ($r->res["status"] == 0) {
            MESSAGE(0, "บัญชีผู้ใช้ยังไม่ได้รับการยืนยัน", "โปรดติดต่อเจ้าหน้าที่เพื่อยืนยัน");
        }
        else {
            SC("id", $r->res["id"]);
            PAGE_PAR("home");
        }
    }

    if ($_GET["m"] == "pass") {
        MESSAGE(1, "สมัครสมาชิกสำเร็จแล้ว", "กรุณารออนุมัติจากเจ้าหน้าที่");
    }
?>
<form class="ui form segment error" id="login" method="POST" action="<?= PAGE("login"); ?>">
    <input type="hidden" name="form" value="login">
    <div class="field">
        <label>อีเมล์</label>
        <input type="email" name="email" placeholder="joe@schmoe.com">
    </div>
    <div class="field">
        <label>รหัสผ่าน</label>
        <input type="password" name="password">
    </div>
    <div class="ui error message"></div>
    <button class="ui button" type="submit">ลงชื่อเข้าใช้</button>
</form>


<script type="text/javascript">
$('.ui.form#login').form({
    fields: {
        email: {identifier: 'email', rules: [{type: 'email', prompt : 'กรุณากรอก อีเมล์'}]},
        password: {identifier: 'password', rules: [{type : 'empty', prompt : 'กรุณากรอก รหัสผ่าน'}, {type : 'minLength[6]', prompt : 'รหัสผ่านอย่างน้อย 6 ตัวอักษร'}]}
    }
});
</script>
