<?php
    if ($_POST["form"] == "content") {
        $title = htmlentities($_POST["title"], ENT_QUOTES);
        $content = htmlentities($_POST["content"], ENT_QUOTES);
        $status = $_POST["status"];
        $pid = $_POST["id"];
        if (isset($pid)) {
            $r = SQL("UPDATE contents SET title = '$title', content = '$content', status = '$status', last_date = NOW(), approved_date = '' WHERE id = $pid AND author_id = $ID");
            if ($r->affected_rows == 1) {
                PAGE_PAR("edit-content", array("m" => "edit", "id" => $pid));
            }
            else {
                MESSAGE(0, "เกิดข้อผิดพลาด", "กรุณาตรวจสอบและลองใหม่อีกครั้ง");
            }
        }
        else {
            $r = SQL("INSERT INTO contents (title, content, author_id, added_date, status) VALUES ('$title', '$content', $ID, NOW(), $status)");
            if ($r->affected_rows == 1) {
                PAGE_PAR("edit-content", array("m" => "new", "id" => $r->insert_id));
            }
            else {
                MESSAGE(0, "เกิดข้อผิดพลาด", "กรุณาตรวจสอบและลองใหม่อีกครั้ง");
            }
        }


    }

    if ($_GET["m"] == "new") {
        MESSAGE(1, "เพิ่มบทความใหม่เรียบร้อยแล้ว");
    }
    if ($_GET["m"] == "edit") {
        MESSAGE(1, "แก้ไขบทความเรียบร้อยแล้ว");
    }

    if (isset($_GET["id"])) {
        $gid = $_GET["id"];
        $r = SQL("SELECT title, content FROM contents WHERE id = $gid AND author_id = $ID", false);
        if ($r->num_rows == 0) {
            MESSAGE(0, "ไม่สามารถเปิดบทความได้", "บทความไม่มีอยู่ หรือบัญชีผู้ใช้นี้ไม่มีสิทธิ์แก้ไข");
        }

        $title = $r->res["title"];
        $content = $r->res["content"];
    }
?>

<div class="ui segment">
    <form class="ui form error" id="content" method="POST" action="<?= PAGE("edit-content"); ?>">
        <input type="hidden" name="form" value="content">
        <?php if ($r->num_rows != 0) : ?>
            <input type="hidden" name="id" value="<?= $gid ?>">
        <?php endif; ?>
        <div class="field">
            <label>หัวข้อ</label>
            <input type="text" name="title" value="<?= html_entity_decode($title) ?>">
        </div>
        <div class="field">
            <textarea name="content" id="mytextarea"><?= html_entity_decode($content) ?></textarea>
        </div>
        <div class="ui error message"></div>
        <button class="ui button" type="submit" name="status" value="0">บันทึกแบบร่าง</button>
        <button class="ui button" type="submit" name="status" value="1">ส่งบทความเพื่อพิจารณา</button>
    </form>
</div>

<script type="text/javascript">

$('.ui.form#content').form({
    fields: {
        title: {identifier: 'title', rules: [{type : 'empty', prompt : 'กรุณาใส่หัวข้อ'}]}
    }
});

tinymce.init({
    selector: '#mytextarea',
    height: 500,
    plugins: [
        "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern imagetools"
    ],

    toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
    toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
    toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",

    imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],

    //menubar: false,
    toolbar_items_size: 'small',

    style_formats: [{
        title: 'Bold text',
        inline: 'b'
    }, {
        title: 'Red text',
        inline: 'span',
        styles: {
            color: '#ff0000'
        }
    }, {
        title: 'Red header',
        block: 'h1',
        styles: {
            color: '#ff0000'
        }
    }, {
        title: 'Example 1',
        inline: 'span',
        classes: 'example1'
    }, {
        title: 'Example 2',
        inline: 'span',
        classes: 'example2'
    }, {
        title: 'Table styles'
    }, {
        title: 'Table row 1',
        selector: 'tr',
        classes: 'tablerow1'
    }],

    pagebreak_separator: '<p class="pagebreak"></p>',

    templates: [{
        title: 'แม่แบบบทความ',
        content: "บทความเกริ่น<br><div class='pagebreak'></div><br>บทความต่อ<br><br>ที่มา : "
    }],
    content_css: [
        'css/codepen.min.css'
    ],

    image_title: true,
    // enable automatic uploads of images represented by blob or data URIs
    automatic_uploads: true,
    // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
    images_upload_url: 'postAcceptor.php',
    file_picker_types: 'image',
    file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

        // Note: In modo not need it anymore.

        input.onchange = function() {
            var file = this.files[0];

            // Note: Nowry, as we are looking to handle it internally.
            var id = 'blobid' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var blobInfo = blobCache.create(id, file);
            blobCache.add(blobInfo);

            cb(blobInfo.blobUri(), { title: file.name });
        };

        input.click();
    }
});
</script>
