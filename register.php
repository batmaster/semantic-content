<?php
    if ($_POST["form"] == "register") {
        $name = $_POST["name"];
        $email = $_POST["email"];
        $password = MD5($_POST["password"]);
        $type = $_POST["type"];
        $r = SQL("INSERT INTO users (name, email, password, type, register_date) VALUES ('$name', '$email', '$password', $type, NOW())");

        if ($r->affected_rows == 1) {
            PAGE_PAR("login", array("m" => "pass"));
        }
        else {
            MESSAGE(0, "สมัครสมาชิกไม่สำเร็จ", "อีเมล์นี้ถูกใช้แล้ว กรุณาลองใหม่");
        }
    }
    
    $r = SQL("SELECT name, email, type FROM users WHERE id = $ID");
?>

<form class="ui form segment error" id="register" method="POST" action="<?= PAGE("register"); ?>">
    <input type="hidden" name="form" value="register">
    <div class="field">
        <label>ชื่อ - นามสกุล</label>
        <input type="text" name="name">
    </div>
    <div class="field">
        <label>อีเมล์</label>
        <input type="email" name="email" placeholder="joe@schmoe.com">
    </div>
    <div class="field">
        <label>รหัสผ่าน</label>
        <input type="password" name="password">
    </div>
    <div class="field">
        <label>ยืนยันรหัสผ่าน</label>
        <input type="password" name="password2">
    </div>
    <div class="field">
        <label>ประเภทผู้ใช้งาน</label>
        <select name="type" class="ui dropdown">
            <option value="">เลือก</option>
            <option value="0">ผู้เขียน</option>
            <option value="1">เจ้าหน้าที่</option>
            <option value="2">บรรณาธิการ</option>
            <option value="3">ผู้บริหาร</option>
            <option value="4">ผู้ทรงคุณวุฒิ</option>
            <option value="5">ผู้เยี่ยมชม</option>
        </select>
    </div>
    <div class="ui error message"></div>
    <button class="ui button" type="submit">สมัครสมาชิก</button>
</form>


<script type="text/javascript">
$('.ui.form#register').form({
    fields: {
        name: {identifier: 'name', rules: [{type : 'empty', prompt : 'กรุณากรอก ชื่อ - นามสกุล'}]},
        email: {identifier: 'email', rules: [{type: 'email', prompt : 'กรุณากรอก อีเมล์'}]},
        password: {identifier: 'password', rules: [{type : 'empty', prompt : 'กรุณากรอก รหัสผ่าน'}, {type : 'minLength[6]', prompt : 'รหัสผ่านอย่างน้อย 6 ตัวอักษร'}]},
        password2: {identifier: 'password2', rules: [{type : 'match[password]', prompt : 'รหัสยืนยันไม่ถูกต้อง'}]},
        type: {identifier: 'type', rules: [{type : 'integer', prompt : 'กรุณาเลือก ประเภทผู้ใช้งาน'}]}
    }
});
</script>
