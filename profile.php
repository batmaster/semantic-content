<?php
    if ($_POST["form"] == "edit") {
        $name = $_POST["name"];
        $email = $_POST["email"];
        $password = MD5($_POST["password"]);
        $old_password = MD5($_POST["old_password"]);
        if ($password == "d41d8cd98f00b204e9800998ecf8427e") {
            $password = "";
        }
        else {
            $password = ", password = '$password'";
        }
        $r = SQL("UPDATE users SET name = '$name', email = '$email' $password WHERE id = $ID AND password = '$old_password'");

        if ($r->affected_rows == 1) {
            MESSAGE(1, "แก้ไขข้อมูลเสร็จเรียบร้อย");
        }
        else {
            MESSAGE(0, "แก้ไขข้อมูลไม่สำเร็จ", "รหัสผ่านเดิมไม่ถูกต้อง");
        }
    }

    $r = SQL("SELECT name, email, type FROM users WHERE id = $ID", false);
?>

<form class="ui form segment error" id="edit" method="POST" action="<?= PAGE("profile"); ?>">
    <input type="hidden" name="form" value="edit">
    <div class="field">
        <label>ชื่อ - นามสกุล</label>
        <input type="text" name="name" value="<?= $r->res["name"] ?>">
    </div>
    <div class="field">
        <label>อีเมล์</label>
        <input type="email" name="email" placeholder="joe@schmoe.com" value="<?= $r->res["email"] ?>">
    </div>
    <div class="field">
        <label>รหัสผ่าน</label>
        <input type="password" name="password">
    </div>
    <div class="field">
        <label>ยืนยันรหัสผ่าน</label>
        <input type="password" name="password2">
    </div>
    <p>ปล่อยว่างหากไม่แก้ไขรหัสผ่าน</p>
    <div class="field">
        <label>ประเภทผู้ใช้งาน</label>
        <select class="ui dropdown">
            <?php if ($r->res["type"] == 0) : ?>
                <option value="0">ผู้เขียน</option>
            <?php elseif ($r->res["type"] == 1) : ?>
                <option value="1">เจ้าหน้าที่</option>
            <?php elseif ($r->res["type"] == 2) : ?>
                <option value="2">บรรณาธิการ</option>
            <?php elseif ($r->res["type"] == 3) : ?>
                <option value="3">ผู้บริหาร</option>
            <?php elseif ($r->res["type"] == 4) : ?>
                <option value="4">ผู้ทรงคุณวุฒิ</option>
            <?php elseif ($r->res["type"] == 5) : ?>
                <option value="5">ผู้เยี่ยมชม</option>
            <?php endif; ?>
        </select>
    </div>
    <div class="ui divider"></div>
    <div class="field">
        <label>ใส่รหัสเดิมเพื่อยืนยันการแก้ไข</label>
        <input type="password" name="old_password">
    </div>
    <div class="ui error message"></div>
    <button class="ui button" type="submit">แก้ไขข้อมูล</button>
</form>


<script type="text/javascript">
$('.ui.form#edit').form({
    fields: {
        name: {identifier: 'name', rules: [{type : 'empty', prompt : 'กรุณากรอก ชื่อ - นามสกุล'}]},
        email: {identifier: 'email', rules: [{type: 'email', prompt : 'กรุณากรอก อีเมล์'}]},
        password: {identifier: 'password', rules: [{type : 'regExp[/^(?=(?:.{0}|.{6,})$).*$/]', prompt : 'รหัสผ่านอย่างน้อย 6 ตัวอักษร หรือปล่อยว่าง'}]},
        password2: {identifier: 'password2', rules: [{type : 'match[password]', prompt : 'รหัสยืนยันไม่ถูกต้อง'}]},
        old_password: {identifier: 'old_password', rules: [{type : 'minLength[6]', prompt : 'รหัสผ่านอย่างน้อย 6 ตัวอักษร'}]}
    }
});
</script>
