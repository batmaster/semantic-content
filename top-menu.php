<?php if ($P == "logout") LOGOUT(); ?>

<div class="ui menu">
    <div class="header item">สวัสดี คุณใคร <?= $NAME ?> </div>
    <a class="item" href="<?= PAGE('home') ?>">หน้าหลัก</a>
    <a class="item" href="<?= PAGE('profile'); ?>">ข้อมูลผู้ใช้</a>
    <a class="item" href="<?= PAGE('contents'); ?>">บทความ</a>
    <a class="item" href="<?= PAGE('news'); ?>">ข่าว</a>
    <a class="item" href="<?= PAGE('files'); ?>">ไฟล์</a>
    <a class="item" href="<?= PAGE('wall') ?>">หน้าบทความ</a>

    <div class="right menu">
        <?php if (isset($ID)) : ?>
            <a class="item" href="<?= PAGE('logout'); ?>">ออกจากระบบ</a>
        <?php else : ?>
            <a class="item" href="<?= PAGE('login'); ?>">เข้าสู่ระบบ</a>
            <a class="item" href="<?= PAGE('register'); ?>">สมัครสมาชิก</a>
        <?php endif; ?>
    </div>
</div>
