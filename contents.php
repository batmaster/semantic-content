<?php

    if ($_POST["form"] == "remove") {
        $p_id = $_POST["id"];
        $r = SQL("DELETE FROM contents WHERE id = $p_id");

        if ($r->affected_rows == 1) {
            MESSAGE(1, "ลบบทความแล้ว");
        }
        else {
            MESSAGE(0, "เกิดข้อผิดพลาดขณะลบบทความ", "กรุณาลองใหม่");
        }
    }

    if ($TYPE == 0) {
        $a_id = $ID;
        $p_id = "%";
    }
    else if ($TYPE == 4) {
        $a_id = "%";
        $p_id = $ID;
    }
    else {
        $a_id = "%";
        $p_id = "%";
    }

    $page = $_GET["p"];
    if (!isset($page)) {
        $page = 1;
    }

    $perpage = $_GET["row"];
    if (!isset($perpage)) {
        $perpage = 20;
    }
    else if ($perpage == 0) {
        $perpage = 10000;
    }

    $r = PAGI("SELECT c.id, c.title, (SELECT u.name FROM users u WHERE u.id = c.author_id) author, c.added_date, c.last_date, c.status, (SELECT u.name FROM users u WHERE u.id = c.professional_id) professional FROM contents c WHERE c.author_id LIKE '$a_id' AND c.professional_id LIKE '$p_id'", $page, $perpage);
    $start = (($page - 1) * $perpage) + 1;
?>


<div class="ui segment">
    <form class="ui form" method="GET" action="<?= PAGE("contents"); ?>">
        <input type="hidden" name="page" value="contents">
        <div class="ui search">

            <div class="ui field action fluid input">
                <input class="prompt" type="text" name="search" placeholder="ข้อความบางส่วน หรื่อชื่อผู้เขียน" value="<?= $_GET["search"] ?>">
                <button class="ui icon button" type="submit" name="order" value="<?= isset($_GET["order"]) ? $_GET["order"] : "added_date|asc" ?>">
                    <i class="search icon"></i>
                </button>
            </div>
            <div class="results"></div>
        </div>

        <p class="right aligned">ผลลัพธ์ <?= $start ?> - <?= ($page * $perpage) ?> จาก <?= $r->num_rows ?></p>
        <table class="ui celled table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>
                        <?php if (preg_match('/^title/', $_GET["order"]) === 1 && preg_match('/desc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="title|asc">หัวข้อ <i class="sort content descending icon"></i></button>
                        <?php elseif (preg_match('/^title/', $_GET["order"]) === 1 && preg_match('/asc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="title|desc">หัวข้อ <i class="sort content ascending icon"></i></button>
                        <?php else : ?>
                            <button class="li" type="submit" name="order" value="title|asc">หัวข้อ</button>
                        <?php endif; ?>
                    </th>
                    <th>
                        <?php if (preg_match('/^author/', $_GET["order"]) === 1 && preg_match('/desc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="author|asc">ผู้เขียน <i class="sort content descending icon"></i></button>
                        <?php elseif (preg_match('/^author/', $_GET["order"]) === 1 && preg_match('/asc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="author|desc">ผู้เขียน <i class="sort content ascending icon"></i></button>
                        <?php else : ?>
                            <button class="li" type="submit" name="order" value="author|asc">ผู้เขียน</button>
                        <?php endif; ?>
                    </th>
                    <th>
                        <?php if (preg_match('/^added_date/', $_GET["order"]) === 1 && preg_match('/desc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="added_date|asc">วันที่เพิ่ม <i class="sort content descending icon"></i></button>
                        <?php elseif (!isset($_GET["order"]) || (preg_match('/^added_date/', $_GET["order"]) === 1 && preg_match('/asc$/', $_GET["order"]) === 1)) : ?>
                            <button class="li" type="submit" name="order" value="added_date|desc">วันที่เพิ่ม <i class="sort content ascending icon"></i></button>
                        <?php else : ?>
                            <button class="li" type="submit" name="order" value="added_date|asc">วันที่เพิ่ม</button>
                        <?php endif; ?>
                    </th>
                    <th>
                        <?php if (preg_match('/^last_date/', $_GET["order"]) === 1 && preg_match('/desc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="last_date|asc">วันที่แก้ไขล่าสุด <i class="sort content descending icon"></i></button>
                        <?php elseif (preg_match('/^last_date/', $_GET["order"]) === 1 && preg_match('/asc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="last_date|desc">วันที่แก้ไขล่าสุด <i class="sort content ascending icon"></i></button>
                        <?php else : ?>
                            <button class="li" type="submit" name="order" value="last_date|asc">วันที่แก้ไขล่าสุด</button>
                        <?php endif; ?>
                    </th>
                    <th>
                        <?php if (preg_match('/^status/', $_GET["order"]) === 1 && preg_match('/desc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="status|asc">สถานะ <i class="sort content descending icon"></i></button>
                        <?php elseif (preg_match('/^status/', $_GET["order"]) === 1 && preg_match('/asc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="status|desc">สถานะ <i class="sort content ascending icon"></i></button>
                        <?php else : ?>
                            <button class="li" type="submit" name="order" value="status|asc">สถานะ</button>
                        <?php endif; ?>
                    </th>
                    <th>
                        <?php if (preg_match('/^professional/', $_GET["order"]) === 1 && preg_match('/desc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="professional|asc">ผู้ทรงคุณวุฒิ <i class="sort content ascending icon"></i></button>
                        <?php elseif (preg_match('/^professional/', $_GET["order"]) === 1 && preg_match('/asc$/', $_GET["order"]) === 1) : ?>
                            <button class="li" type="submit" name="order" value="professional|desc">ผู้ทรงคุณวุฒิ <i class="sort content ascending icon"></i></button>
                        <?php else : ?>
                            <button class="li" type="submit" name="order" value="professional|asc">ผู้ทรงคุณวุฒิ</button>
                        <?php endif; ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php for($i = 0; $i < count($r->res); $i++): ?>
                    <tr>
                        <td><?= $i + $start ?></td>
                        <td><a href="<?= GET_PAGE_PAR("content-detail", array("id" => "{$r->res[$i]['id']}")) ?>"><?= $r->res[$i]["title"] ?></a></td>
                        <td><?= $r->res[$i]["author"] ?></td>
                        <td><?= $r->res[$i]["added_date"] ?></td>
                        <td><?= $r->res[$i]["last_date"] ?></td>
                        <td><?= ContentStatus::getStatusText($r->res[$i]['id']) ?></td>
                        <td><?= $r->res[$i]["professional"] ?></td>
                    </tr>
                <?php endfor; ?>
            </tbody>
            <tfoot>
                <tr><th colspan="7">
                    <div class="ui left icon input">
                        <select name="row" class="ui dropdown">
                            <option value="20" <?= !isset($_GET["row"]) || $_GET["row"] == "20" ? "selected" : "" ?>>20</option>
                            <option value="50" <?= $_GET["row"] == "50" ? "selected" : "" ?>>50</option>
                            <option value="100" <?= $_GET["row"] == "100" ? "selected" : "" ?>>100</option>
                            <option value="0" <?= $_GET["row"] == "0" ? "selected" : "" ?>>ทั้งหมด</option>
                        </select>

                        <button class="ui icon button" type="submit" name="order" value="<?= isset($_GET["order"]) ? $_GET["order"] : "added_date|asc" ?>">
                            <i class="list layout icon"></i>
                        </button>

                    </div>
                    <?php
                        $all_page = ceil($r->num_rows/$perpage);
                        $start = 0;
                        $end = 0;
                        if ($all_page < 5) {
                            $start = 1;
                            $end = $all_page;
                        }

                    ?>
                    <div class="ui right floated pagination menu">
                        <a class="icon item"><i class="angle double left icon"></i></a>
                        <a class="icon item"><i class="angle left icon"></i></a>
                        <?php for($i = 1; $i <= $all_page; $i++): ?>
                            <a class="item"><?= $i ?></a>
                        <?php endfor; ?>
                        <a class="icon item"><i class="angle right icon"></i></a>
                        <a class="icon item"><i class="angle double right icon"></i></a>
                    </div>
                </th>
            </tr></tfoot>
        </table>

    </form>

</div>

<script type="text/javascript">
var content = [
    { title: 'Andorra' },
    { title: 'United Arab Emirates' },
    { title: 'Afghanistan' },
    { title: 'Antigua' },
    { title: 'Anguilla' },
    { title: 'Albania' },
    { title: 'Armenia' },
    { title: 'Netherlands Antilles' },
    { title: 'Angola' },
    { title: 'Argentina' },
    { title: 'American Samoa' },
    { title: 'Austria' },
    { title: 'Australia' },
    { title: 'Aruba' },
    { title: 'Aland Islands' },
    { title: 'Azerbaijan' },
    { title: 'Bosnia' },
    { title: 'Barbados' },
    { title: 'Bangladesh' },
    { title: 'Belgium' },
    { title: 'Burkina Faso' },
    { title: 'Bulgaria' },
    { title: 'Bahrain' },
    { title: 'Burundi' }
];

$('.ui.search').search({
    source: content
});

</script>
