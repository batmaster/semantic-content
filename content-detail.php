<?php
    $g_id = $_GET["id"];

    if ($_POST["form"] == "professional") {
        $id = $_POST["id"];
        $r = SQL("UPDATE contents SET professional_id = $id WHERE id = $g_id");

        if ($r->affected_rows == 1) {
            MESSAGE(1, "กำหนดผู้ทรงคุณวุฒิเรียบร้อยแล้ว");
        }
        else {
            MESSAGE(0, "เกิดข้อผิดพลาดขณะกำหนดผู้ทรงคุณวุฒิ", "กรุณาลองใหม่");
        }
    }
    else if ($_POST["form"] == "approve") {
        $r = SQL("UPDATE contents SET approved_date = NOW() WHERE id = $g_id");

        if ($r->affected_rows == 1) {
            MESSAGE(1, "ประเมินผลเรียบร้อยแล้ว");
        }
        else {
            MESSAGE(0, "เกิดข้อผิดพลาดขณะประเมินผล", "กรุณาลองใหม่");
        }
    }
    else if ($_POST["form"] == "publish") {
        $id = $_POST["id"];
        $r = SQL("UPDATE contents SET publish_date = NOW() WHERE id = $g_id");

        if ($r->affected_rows == 1) {
            MESSAGE(1, "เผยแพร่บทความเรียบร้อยแล้ว");
        }
        else {
            MESSAGE(0, "เกิดข้อผิดพลาดขณะเผยแพร่บทความ", "กรุณาลองใหม่");
        }
    }

    $r = SQL("SELECT ct.id, ct.title, ct.content, (SELECT u.name FROM users u WHERE u.id = ct.author_id) author, ct.added_date, ct.last_date, ct.count, ct.due_date,
    (SELECT u.name FROM users u WHERE u.id = ct.professional_id) professional, ct.approved_date, ct.publish_date, (SELECT COUNT(*) FROM comments WHERE content_id = $g_id) comments
    FROM contents ct WHERE ct.id = $g_id", false);

    $professional = SQL("SELECT id, name FROM users WHERE type = 4 AND status = 1");

    $c = SQL("SELECT (SELECT u.name FROM users u WHERE u.id = co.commentator_id) name, co.content, co.added_date FROM comments co WHERE co.content_id = $g_id");

    $st = ContentStatus::getStatus($g_id);
?>

<div class="ui stacked segment">
    <h2><?= $r->res["title"] ?></h2>
</div>

<div class="ui segment">
    <div class="ui cards">
        <div class="card">
            <div class="content">
                <div class="header">ผู้เขียน</div>
                <div class="description">
                    <?= $r->res["author"] ?>
                </div>
            </div>

            <div class="content">
                <div class="header">วันที่เพิ่ม</div>
                <div class="description">
                    <?= $r->res["added_date"] ?>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="content">
                <div class="header">วันที่แก้ไขล่าสุด</div>
                <div class="description">
                    <?= $r->res["last_date"] == "" ? "-" : $r->res["last_date"] ?>
                </div>
            </div>

            <div class="content">
                <div class="header">วันที่กำหนดส่ง</div>
                <div class="description">
                    <?= $r->res["due_date"] == "" ? "-" : $r->res["due_date"] ?>
                </div>
            </div>

            <?php if ($st == 0 || $st == 1 || $st == 100 || $st == 101) : ?>
                <div class="extra content">
                    <div class="ui two buttons">
                        <a class="ui basic green button" href="<?= GET_PAGE_PAR("calendar", array("focus" => $g_id)) ?>">
                            <i class="calendar icon"></i>กำหนดวันที่ส่ง
                        </a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="card">
            <div class="content">
                <div class="header">ผู้ทรงคุณวุฒิ</div>
                <div class="description">
                    <?= $r->res["professional"] == "" ? "-" : $r->res["professional"] ?>
                </div>
            </div>

            <?php if ($st == 0 || $st == 10 || $st == 100 || $st == 110) : ?>
                <div class="extra content">

                    <form method="POST" id="professional" class="ui form" action="<?= GET_PAGE_PAR("content-detail", array("id" => $g_id)); ?>">
                        <input type="hidden" name="form" value="professional">
                        <div class="field">
                            <select name="id" class="ui dropdown">
                                <option value="">เลือก</option>
                                <?php for($i = 0; $i < count($professional->res); $i++) : ?>
                                    <option value="<?= $professional->res[$i]["id"] ?>"><?= $professional->res[$i]["name"] ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>

                        <div class="ui two buttons">
                            <div onclick="$('#confirm-professional-modal').modal('show');" class="ui basic green button">กำหนดผู้ทรงคุณวุฒิ</div>
                        </div>
                    </form>
                </div>
            <?php endif; ?>

            <div class="content">
                <div class="header">วันที่พิจารณาผ่าน</div>
                <div class="description">
                    <?= $r->res["approved_date"] == "" ? "-" : $r->res["approved_date"] ?>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="content">
                <div class="header">สถานะ</div>
                <div class="description">
                    <?= ContentStatus::$STATUS[$st]; ?>
                </div>

                <?php if ($st == 111) : ?>
                    <div class="extra content">
                        <div class="ui two buttons">
                            <div onclick="$('#confirm-approve-modal').modal('show');" class="ui basic green button">ประเมินผลผ่าน</div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($st == 2) : ?>
                    <div class="extra content">
                        <div class="ui two buttons">
                            <div onclick="$('#confirm-publish-modal').modal('show');" class="ui basic green button">เผยแพร่บทความ</div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="content">
                <div class="header">วันที่เผยแพร่บทความ</div>
                <div class="description">
                    <?= $r->res["publish_date"] == "" ? "-" : $r->res["publish_date"] ?>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="content">
                <div class="header">จำนวนครั้งเปิดอ่าน</div>
                <div class="description">
                    <?= $r->res["count"] ?>
                </div>
            </div>

            <div class="content">
                <div class="header">จำนวนความเห็นบทความ</div>
                <div class="description">
                    <?= $r->res["comments"] ?>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="content">
                <div class="ui two buttons">
                    <a class="ui green button" href="<?= GET_PAGE_PAR("edit-content", array("id" => $g_id)) ?>">
                        <i class="edit icon"></i>แก้ไขบทความ
                    </a>
                </div>
            </div>

            <div class="content">
                <div class="ui two buttons">
                    <div onclick="$('#confirm-remove-modal').modal('show');" class="ui red button"><i class="trash outline icon"></i>ลบบทความ</div>
                </div>
            </div>
        </div>
    </div>

    <div class="ui ordered steps">
        <div class="ui vertical steps">
            <div class="<?= $st == 100 || $st == 101 || $st == 110 || $st == 111 || $st == 2 || $st == 3 ? "completed" : "active" ?> step">
                <div class="content">
                    <div class="title">เขียนบทความ</div>
                    <div class="description">ผู้เขียน</div>
                </div>
            </div>
            <div class="<?= $st == 10 || $st == 11 || $st == 110 || $st == 111 || $st == 2 || $st == 3 ? "completed" : "active" ?> step">
                <div class="content">
                    <div class="title">กำหนดวันที่ส่ง</div>
                    <div class="description">เจ้าหน้าที่</div>
                </div>
            </div>
            <div class="<?= $st == 1 || $st == 11 || $st == 101 || $st == 111 || $st == 2 || $st == 3 ? "completed" : "active" ?> step">
                <div class="content">
                    <div class="title">กำหนดผู้ทรงคุณวุฒิ</div>
                    <div class="description">บรรณาธิการ</div>
                </div>
            </div>
        </div>
        <div class="<?= $st == 111 || $st == 2 || $st == 3 ? "completed" : "disable" ?> step">
            <div class="content">
                <div class="title">ส่งบทความ</div>
                <div class="description">ผู้เขียน</div>
            </div>
        </div>
        <div class="<?= $st == 111 ? "active" : ($st == 2 || $st == 3 ? "completed" : "disable") ?> step">
            <div class="content">
                <div class="title">ประเมินบทความ</div>
                <div class="description">ผู้ทรงคุณวุฒิ</div>
            </div>
        </div>
        <div class="<?= $st == 3 ? "completed" : ($st == 2 ? "active" : "disable") ?> step">
            <div class="content">
                <div class="title">เผยแพร่บทความ</div>
                <div class="description">บรรณาธิการ</div>
            </div>
        </div>
    </div>
</div>

<div class="ui segment">
    <?= html_entity_decode($r->res["content"]) ?>
</div>

<div class="ui segment">
    <div class="ui comments" style="max-width: 100%;">
        <h3 class="ui dividing header">ความคิดเห็น</h3>

        <?php for ($i = 0; $i < count($c->res); $i++) : ?>
            <div class="comment">
                <div class="content">
                    <a class="author"><?= $c->res[$i]["name"] ?></a>
                    <div class="metadata">
                        <span class="date"><?= $c->res[$i]["added_date"] ?></span>
                    </div>
                    <div class="text">
                        <?= $c->res[$i]["content"] ?>
                    </div>
                </div>
            </div>
        <?php endfor; ?>
    </div>

<div class="ui modal" id="confirm-professional-modal">
    <div class="header">ยันยันการกำหนดผู้ทรงคุณวุฒิ</div>
    <div class="content">
        <p>เมื่อกำหนดผู้ทรงคุณวุฒิแล้ว จะไม่สามารถเปลี่ยนได้</p>
    </div>
    <div class="actions">
        <div class="ui red cancel button">ยกเลิก</div>
        <div onclick="$('#professional').submit();" class="ui green approve button">ยืนยัน</div>
    </div>
</div>

<form method="POST" id="approve" class="ui form" action="<?= GET_PAGE_PAR("content-detail", array("id" => $g_id)); ?>">
    <input type="hidden" name="form" value="approve">
</form>
<div class="ui modal" id="confirm-approve-modal">
    <div class="header">ยันยันการประเมินผล</div>
    <div class="content">
        <p>เมื่อประเมินผลผ่านแล้ว จะไม่สามารถเปลี่ยนได้</p>
    </div>
    <div class="actions">
        <div class="ui red cancel button">ยกเลิก</div>
        <div onclick="$('#approve').submit();" class="ui green approve button">ยืนยัน</div>
    </div>
</div>

<form method="POST" id="publish" class="ui form" action="<?= GET_PAGE_PAR("content-detail", array("id" => $g_id)); ?>">
    <input type="hidden" name="form" value="publish">
</form>
<div class="ui modal" id="confirm-publish-modal">
    <div class="header">ยันยันการเผยแพร่บทความ</div>
    <div class="content">
        <p>เมื่อเผยแพร่บทความ่แล้ว จะไม่สามารถเปลี่ยนได้</p>
    </div>
    <div class="actions">
        <div class="ui red cancel button">ยกเลิก</div>
        <div onclick="$('#publish').submit();" class="ui green approve button">ยืนยัน</div>
    </div>
</div>

<form method="POST" id="remove" class="ui form" action="<?= GET_PAGE_PAR("contents", array("id" => $g_id)); ?>">
    <input type="hidden" name="form" value="remove">
    <input type="hidden" name="id" value="<?= $g_id ?>">
</form>
<div class="ui modal" id="confirm-remove-modal">
    <div class="header">ยันยันการลบบทความ</div>
    <div class="content">
        <p>เมื่อลบบทความ่แล้ว จะไม่สามารถเปลี่ยนได้</p>
    </div>
    <div class="actions">
        <div class="ui red cancel button">ยกเลิก</div>
        <div onclick="$('#remove').submit();" class="ui green approve button">ยืนยัน</div>
    </div>
</div>
