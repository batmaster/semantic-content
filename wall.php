<?php
date_default_timezone_set('Asia/Bangkok');

    // TODO PAGI later
    $r = SQL("SELECT ct.id, ct.title, ct.content, (SELECT u.name FROM users u WHERE u.id = ct.author_id) author, ct.publish_date FROM contents ct
    WHERE ct.status = 1 AND ct.due_date != '' AND ct.professional_id != 0 AND ct.approved_date != '' AND ct.publish_date != ''");

?>
<style type="text/css">
.image-content {
    width: 200px;
    height: 200px;
    overflow: hidden;
    object-fit: cover;
    float: left;
    margin: 10px;
    margin-right: 20px;
    border-radius: 4px;
}
</style>
<div class="ui container">
    <div class="ui segment">
        <?php for($i = 0; $i < count($r->res); $i++) : ?>
            <div class="ui card" style="width: 100%;">
                <div class="content">
                    <div class="header"><a href="<?= GET_PAGE_PAR("read", array("id" => $r->res[$i]["id"])) ?>"><?= $r->res[$i]["title"] ?></a></div>
                    <div class="meta"><?= $r->res[$i]["author"] ?></div>
                    <div class="meta"><?= $r->res[$i]["publish_date"] ?></div>
                    <img class="image-content" src="http://placehold.it/200x200">
                    <div class="description" style="display: inline;">
                        <?= html_entity_decode($r->res[$i]["content"]) ?>
                    </div>
                    <a class="ui right floated primary button" href="<?= GET_PAGE_PAR("read", array("id" => $r->res[$i]["id"])) ?>" style="margin-top: 20px">อ่านต่อ</a>
                </div>
            </div>
        <?php endfor; ?>
    </div>
</div>

<script type="text/javascript">
    $(".pagebreak").nextAll().remove();

    $(".description").each(function() {
        var src = $(this).find('img:first').attr("src");
        var image = $(this).prev();
        console.log(image);
        if (src == undefined) {
            image.remove();
        }
        else {
            image.attr("src", src);
        }
    });

    $(".pagebreak").remove();
    $(".description img").remove();
    $(".description iframe").remove();

</script>
