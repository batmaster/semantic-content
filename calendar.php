<?php
    $not_events = SQL("SELECT c.id, (SELECT u.name FROM users u WHERE u.id = c.author_id) author, c.title FROM contents c WHERE c.due_date = ''");
    $events = SQL("SELECT c.id, (SELECT u.name FROM users u WHERE u.id = c.author_id) author, c.title, c.due_date FROM contents c WHERE c.due_date != ''");

    $overdue = SQL("SELECT c.id, (SELECT u.name FROM users u WHERE u.id = c.author_id) author, c.title, c.due_date, c.status FROM contents c WHERE DATE(c.due_date) <= NOW() ORDER BY c.due_date DESC");

    $focus_id = $G["focus"];
?>

<?php if ($overdue->num_rows > 0) : ?>
    <div class="ui segment">
        <h4 class="ui dividing header">บทความล่าช้า</h4>
        <table class="ui celled table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>หัวข้อ</th>
                    <th>ผู้เขียน</th>
                    <th>วันที่กำหนดส่ง</th>
                    <th>สถานะ</th>
                </tr>
            </thead>
            <tbody>
                <?php for($i = 0; $i < count($overdue->res); $i++): ?>
                    <tr>
                        <td><?= $i + 1 ?></td>
                        <td><a href="<?= GET_PAGE_PAR("content-detail", array("id" => "{$overdue->res[$i]['id']}")) ?>"><?= $overdue->res[$i]["title"] ?></a></td>
                        <td><?= $overdue->res[$i]["author"] ?></td>
                        <td><?= $overdue->res[$i]["due_date"] ?></td>
                        <td><?php switch ($overdue->res[$i]["status"]) {case 0 : echo "กำลังเขียน"; break; case 1 : echo "ส่งพิจารณา"; break; case 3 : echo "เผยแพร่"; break;} ?></td>
                    </tr>
                <?php endfor; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>

<div class="ui segment">
    <h4 class="ui dividing header">จัดการวันกำหนดส่งบทความ</h4>
    <?phpMESSAGE(1, "แก้ไขวันกำหนดส่งเรียบร้อยแล้ว", "อัพเดตเป็นวันที่ <span id='date'></span>", "message", true); ?>
    <div class="ui segment" id='wrap'>
        <div id='external-events'>
            <h4>บทความใหม่</h4>
            <?php if (isset($focus_id)) : ?>
                <?php for($i = 0; $i < count($not_events->res); $i++) : ?>
                    <?php if ($not_events->res[$i]["id"] == $focus_id) : ?>
                        <div class='fc-event' id="card<?= $not_events->res[$i]["id"] ?>" data-id="<?= $not_events->res[$i]["id"] ?>" data-title="<?= $not_events->res[$i]["author"] . "\n\n" . $not_events->res[$i]["title"] ?>">
                            <div class="ui link red cards">
                                <div class="card">
                                    <div class="content">
                                        <div class="meta">
                                            <?= $not_events->res[$i]["author"] ?>
                                        </div>
                                        <div class="description">
                                            <?= $not_events->res[$i]["title"] ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ui divider"></div>
                    <?php endif; ?>
                <?php endfor; ?>
            <?php endif; ?>

            <?php for($i = 0; $i < count($not_events->res); $i++) : ?>
                <?php if (!isset($focus_id) || $not_events->res[$i]["id"] != $focus_id) : ?>
                    <div class='fc-event' id="card<?= $not_events->res[$i]["id"] ?>" data-id="<?= $not_events->res[$i]["id"] ?>" data-title="<?= $not_events->res[$i]["author"] . "\n\n" . $not_events->res[$i]["title"] ?>">
                        <div class="ui link cards">
                            <div class="card">
                                <div class="content">
                                    <div class="meta">
                                        <?= $not_events->res[$i]["author"] ?>
                                    </div>
                                    <div class="description">
                                        <?= $not_events->res[$i]["title"] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endfor; ?>

        </div>

        <div id='calendar'></div>
        <div style='clear:both'></div>
    </div>
</div>

<script>
	$(document).ready(function() {
		$('#external-events .fc-event').each(function() {
			$(this).data('event', {
				title: $(this).data("title"),
				stick: true,
                c_id: $(this).data("id")
			});

			$(this).draggable({
				zIndex: 999,
				revert: true,
				revertDuration: 1000
			});
		});

		$('#calendar').fullCalendar({
			header: {
				left: 'prevYear,prev,next,nextYear today',
				center: 'title',
                right: 'month, listYear'
            },
            buttonIcons: {
                prev: 'left-single-arrow',
                next: 'right-single-arrow',
                prevYear: 'left-double-arrow',
                nextYear: 'right-double-arrow'
            },
            editable: true,
            droppable: true,
            eventLimit: true,
            displayEventTime: false,
            events: [
                <?php for($i = 0; $i < count($events->res); $i++) : ?>
                    {
                        title: '<?= $events->res[$i]["author"] . "\\n\\n" . $events->res[$i]["title"] ?>',
                        start: '<?= $events->res[$i]["due_date"] ?>',
                        c_id: '<?= $events->res[$i]["id"] ?>'
                    },
                <?php endfor; ?>
            ],
            drop: function(date, jsEvent, ui, resourceId) {
                $(this).remove();

                $.ajax({
                    url: "for-calendar.php",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "function": "update",
                        "id": $(this).data("id"),
                        "date": date.format("YYYY-MM-DD")
                    }
                }).done(function(response) {
                    if (response.affected_rows == 1) {
                        if ($('#message').hasClass("visible")) {
                            $('#message').transition('fade');
                        }
                        $("#date").text(date.format("YYYY-MM-DD"));
                        $('#message').transition('fade').show();
                    }
                });
            },
            eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {
                // if (!confirm("Are you sure about this change?")) {
                //     revertFunc();
                // }
                // else {
                    $.ajax({
                        url: "for-calendar.php",
                        type: "POST",
                        dataType: "json",
                        data: {
                            "function": "update",
                            "id": event.c_id,
                            "date": event.start.format("YYYY-MM-DD")
                        }
                    }).done(function(response) {
                        if (response.affected_rows == 1) {
                            if ($('#message').hasClass("visible")) {
                                $('#message').transition('fade');
                            }
                                $("#date").text(event.start.format("YYYY-MM-DD"));
                            $('#message').transition('fade').show();
                        }
                    });
                // }

            }
		});


	});

</script>
