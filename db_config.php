<?php
// config db only here
$HOST = "th.batmasterio.com";
$USER = "root";
$PASS = "rootaemysql";
$DB = "semantic_content";

function SQL($sql, $asArray = true) {
    global $HOST, $USER, $PASS, $DB;

    $db = new mysqli($HOST, $USER, $PASS, $DB);
    mysqli_set_charset($db, "utf8");
    if ($db->connect_errno > 0) {
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    $res = $db->query($sql);
    $result = null;

    if ($res != null && !is_bool($res)) {
        $result = array();
        while ($r = $res->fetch_assoc()) {
            $result[] = $r;
        }

        if (!$asArray) {
            $result = $result[0];
        }
    }
    $r = new Result($result, $sql, $db->affected_rows, $db->errno, $db->error, $db->insert_id, $res->num_rows);
    $db->close();

    return $r;
}

function PAGI($sql, $page, $perpage) {
    global $HOST, $USER, $PASS, $DB;

    $db = new mysqli($HOST, $USER, $PASS, $DB);
    if ($db->connect_errno > 0) {
        die('Unable to connect to database [' . $db->connect_error . ']');
    }

    $offset = ($page - 1) * $perpage;
    $new_sql = "$sql LIMIT $perpage OFFSET $offset";

    $res = $db->query($new_sql);
    if ($res != undefined && !is_bool($res)) {
        $result = array();
        while ($r = $res->fetch_assoc()) {
            $result[] = $r;
        }
    }
    $r = new Result($result, $new_sql, $db->affected_rows, $db->errno, $db->error, $db->insert_id, $res->num_rows);

    $r->num_rows = $db->query("$sql")->num_rows;
    $db->close();

    return $r;
}

?>
