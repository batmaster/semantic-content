<div class="ui vertical menu">
  <a class="item" href="<?= PAGE('profile'); ?>">ข้อมูลส่วนตัว</a>
  <a class="item" href="<?= PAGE('writer'); ?>">ข้อมูลผู้เขียน</a>
  <a class="item" href="<?= PAGE('staff'); ?>">ข้อมูลเจ้าหน้าที่</a>
  <a class="item" href="<?= PAGE('editor'); ?>">ข้อมูลบรรณาธิการ</a>
  <a class="item" href="<?= PAGE('manager'); ?>">ข้อมูลผู้บริหาร</a>
  <a class="item" href="<?= PAGE('professional'); ?>">ข้อมูลผู้ทรงคุณวุฒิ</a>
  <a class="item" href="<?= PAGE('visitor'); ?>">ข้อมูลผู้เยี่ยมชม</a>
  <a class="item" href="<?= PAGE('contents'); ?>">รายการบทความ</a>
  <a class="item" href="<?= PAGE('edit-content'); ?>">เพิ่มบทความใหม่</a>
  <a class="item" href="<?= PAGE('calendar'); ?>">กำหนดส่งบทความ</a>
  <a class="item" href="<?= PAGE('news'); ?>">รายการข่าว</a>
  <a class="item" href="<?= PAGE('new-news'); ?>">เพิ่มข่าวใหม่</a>
  <a class="item" href="<?= PAGE('files'); ?>">รายการไฟล์</a>
  <a class="item" href="<?= PAGE('new-file'); ?>">เพิ่มไฟล์ใหม่</a>
</div>
