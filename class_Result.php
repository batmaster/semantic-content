<?php
class Result {
    public $res;
    public $sql;
    public $affected_rows;
    public $errno;
    public $error;
    public $insert_id;
    public $num_rows;

    public function __construct($re, $s, $ar, $en, $er, $i, $n) {
        $this->res = $re;
        $this->sql = $s;
        $this->affected_rows = $ar;
        $this->errno = $en;
        $this->error = $er;
        $this->insert_id = $i;
        $this->num_rows = $n;
    }
}
?>
